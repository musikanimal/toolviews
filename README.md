# Toolviews

Visualizes usage of tools on [Toolforge](https://toolforge.org/).

## Contributing

### Dependencies

1. [Node](https://nodejs.org/en/) with the version specified by [.nvmrc](.nvmrc).

### Setup

1. Install all node packages and dependencies with `npm install`.
2. Watch and compile changes with `npm run watch`
3. Open dist/index.html in your browser
