import $ from 'jquery';
import Chart from 'chart.js/auto';
import 'bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap-utilities.css';
import 'select2';
import './styles.scss';
import config from './config';

class Toolviews {
	API_URL = 'https://toolviews.toolforge.org/api/v1';

	CHART_WATERFALL = 'waterfall'
	CHART_PIE = 'pie'
	CHART_POLAR = 'polar'
	CHART_LINE = 'line'
	CHART_BAR = 'bar'
	CHART_RADAR = 'radar'

	constructor() {
		// Parse URL params
		const params = new URLSearchParams(location.search);
		const getParam = (key, defaultValue, allowedValues) => {
			if (params.has(key) && allowedValues) {
				return allowedValues.includes(params.get(key)) ? params.get(key) : defaultValue;
			} else if (params.has(key)) {
				return params.get(key) || defaultValue;
			} else {
				return defaultValue;
			}
		};

		this.tools = params.has('tools') && !!params.get('tools') ? params.get('tools').split('|') : [];
		this.singleDate = params.has('singleDate') ? params.get('singleDate') === '1' : true;
		this.metric = getParam('metric', 'hits', ['hits', 'visitors']);
		this.chartType = getParam('chartType', 'waterfall',
			this.singleDate
				? [this.CHART_WATERFALL, this.CHART_PIE, this.CHART_POLAR]
				: [this.CHART_WATERFALL, this.CHART_PIE, this.CHART_POLAR, this.CHART_LINE, this.CHART_BAR, this.CHART_RADAR]
		);

		// Set default dates, with start date 30 days ago.
		const yesterday = new Date(Date.now() - 864e5);
		const getDate = (key, defaultValue) => {
			let date = getParam(key, defaultValue);
			if (!date.match(/\d{4}-\d{2}-\d{2}/) || Date.parse(date) > Date.now() || date < Date.parse('2018-04-29')) {
				date = defaultValue;
			}
			return date;
		}

		this.date = getDate('date', this.formatDate(yesterday));
		// Defaults to one month ago.
		this.startDate = getDate('start', this.formatDate(new Date(new Date().setDate(yesterday.getDate() - 30))));
		// End date defaults to yesterday, since today's data may be incomplete.
		this.endDate = getDate('end', this.formatDate(yesterday));

		this.max = 0;
		this.chart = null;
		this.prevChartType = this.chartType;

		// Cache a few jQuery selectors.
		this.$outputTable = $('#output-table');
		this.$output = $('#output');
		this.$dateType = $('#date-type');
		this.$chartType = $('#chart-type');
		this.$chart = $('#chart');
		this.$chartWrapper = $('#chart-wrapper');
		this.$toolsInput = $('#tools-input');
		this.$summaryTable = $('#summary-table');
		this.$singleToolSummary = $('#single-tool-summary');
		this.$summaryOutput = $('#summary-table--output');
	}

	init() {
		this.$dateType.val(this.singleDate ? 'single' : 'range');
		$('#single-date-selector').toggle(this.singleDate);
		$('#multi-date-selector').toggle(!this.singleDate);
		// Set max date to today.
		$('#date-input, #start-date').attr('max', new Date());
		// Set default date values.
		$('#date-input').val(this.date);
		$('#start-date').val(this.startDate);
		$('#end-date').val(this.endDate);
		this.$chartType.val(this.chartType);

		fetch(this.API_URL + '/tools')
			.then(res => res.json())
			.then(data => {
				this.$toolsInput.select2({
					placeholder: 'Click and type to only show data for specific tools.',
					data: data.results.map(tool => {
						return {
							id: tool,
							text: tool,
						}
					}),
					allowClear: true,
				});
				if (this.tools) {
					this.$toolsInput.val(this.tools);
					this.$toolsInput.trigger('change');
				}
				this.addListeners();
				this.execute(true);
			}).catch(e => {
				if (!location.host) {
					throw e;
				}
				// TODO: show error
			});
	}

	/**
	 * Format a date as YYYY-MM-DD
	 * @param {Date} date
	 * @return {String}
	 */
	formatDate(date) {
		return date.toISOString().substring(0, 10);
	}

	/**
	 * Add listeners to inputs and update class properties and the view accordingly.
	 */
	addListeners() {
		this.$dateType.on('change', e => {
			this.singleDate = e.target.value === 'single';
			$('#single-date-selector').toggle(this.singleDate);
			$('#multi-date-selector').toggle(!this.singleDate);
			this.execute();
		});
		['#date-input', '#start-date', '#end-date'].forEach(selector => {
			let prevValue = $(selector).val(),
				timer;
			const onChange = e => {
				if (e.target.value === prevValue) {
					return;
				}
				const isInvalid = !e.target.value || !e.target.checkValidity();
				$(e.target).toggleClass('is-invalid', isInvalid);
				if (isInvalid) {
					return;
				}

				if (selector === '#date-input') {
					this.date = e.target.value;
				} else if (selector === '#start-date') {
					this.startDate = e.target.value;
				} else {
					this.endDate = e.target.value;
				}

				prevValue = e.target.value;

				this.execute();
			};
			$(selector).on('change', e => {
				clearTimeout(timer);
				timer = setTimeout(onChange.bind(this, e), 1000);
			});
			$(selector).on('blur', onChange);
		});
		$('#metric').on('change', e => {
			this.metric = e.target.value;
			this.execute();
		});
		this.$chartType.on('change', e => {
			this.chartType = e.target.value;
			// Line, bar and radar require a date range.
			if (['line', 'bar', 'radar'].includes(this.chartType) && this.singleDate) {
				this.$dateType.val('range')
					.trigger('change');
			}
			this.execute();
		});
		this.$toolsInput.on('select2:select', function (e) {
			const $element = $(e.params.data.element);
			$element.detach();
			$(this).append($element);
			$(this).trigger('change');
		});
		this.$toolsInput.on('change', e => {
			this.tools = $(e.target).val();
			this.execute();
		});
	}

	/**
	 * The main entry point to "do stuff". This should be called whenever any options change
	 * and the chart/output needs to be updated.
	 * @param {boolean} [force]
	 */
	execute(force = false) {
		const oldQueryStr = location.search,
			queryStr = this.pushParams();

		// Prevent unnecessarily requery-ing the API.
		if (!force && queryStr === oldQueryStr) {
			return;
		}

		this.setLoading(true);
		const promises = [];

		if (this.tools.length) {
			this.$toolsInput.val().forEach(tool => {
				promises.push(fetch(this.getEndpoint(tool)));
			});
		} else {
			// Uses the all-tools endpoint.
			promises.push(fetch(this.getEndpoint()));
		}

		Promise.all(promises).then(responses => {
			return Promise.all(responses.map(res => res.json() ));
		}).then(data => {
			const results = this.buildChartData(data)
			this.showData(results);
		}).catch(e => {
			this.setLoading(false);
			if (!location.host) {
				// localhost
				throw e;
			}
			// TODO: show error
		});
	}

	/**
	 * Takes one or more results from the Toolviews API
	 * and puts the data in the format that Chart.js needs on
	 * initialization.
	 * @param {Array<Object>} data Responses as JSON from execute() method.
	 * @return {Object} Ready to be passed to Chart.js constructor, or showDataWaterfall().
	 */
	buildChartData(data) {
		switch (this.chartType) {
			case this.CHART_WATERFALL:
				return this.buildDataForWaterfall(data);
			case this.CHART_POLAR:
			case this.CHART_PIE:
				return this.buildDataForPiePolarChart(data);
			case this.CHART_LINE:
			case this.CHART_BAR:
				return this.buildDataForLineBarChart(data);
			case this.CHART_RADAR:
				return this.buildDataForRadarChart(data);
		}
	}

	/**
	 * Build the data structure needed to create the waterfall chart.
	 * @param {Array<Object>} data
	 * @return {Object}
	 */
	buildDataForWaterfall(data) {
		let results = {};
		data.forEach(datum => {
			// Sort by hits
			if (this.singleDate) {
				if (datum.tool === '*') {
					results = this.sortByValues(datum.results);
					return;
				}
				results[datum.tool] = datum.results[datum.tool];
			} else {
				Object.keys(datum.results).forEach(date => {
					Object.keys(datum.results[date]).forEach(tool => {
						// Keys should be tool names, values are hits.
						results[tool] = (results[tool] || 0) + datum.results[date][tool];
					});
				});
				// Sort by hit count.
				results = this.sortByValues(results);
			}
		});
		return results;
	}

	/**
	 * Build the data structure needed to create a bar or line chart in Chart.js.
	 * @param {Array<Object>} data
	 * @return {Object} Ready to be passed to Chart.js constructor.
	 */
	buildDataForLineBarChart(data) {
		const labels = this.getChartDateLabels();
		let datasets = [];
		data.forEach((datum, index) => {
			const values = new Array(labels.length).fill(0),
				color = config.chartColors[index % config.chartColors.length];
			let tool = datum.tool;

			Object.keys(datum.results).forEach(date => {
				const index = labels.indexOf(date);
				values[index] = tool === '*'
					// We want the sum of hits across all tools on the given date.
					? Object.values(datum.results[date]).reduce((partialSum, a) => partialSum + a, 0)
					// There are multiple datasets, so we store the individual hits for each tool.
					: datum.results[date][tool]
			});

			datasets.push({
				label: tool,
				data: values,
				borderColor: color,
				backgroundColor: color,
				sum: values.reduce((partialSum, a) => partialSum + a, 0)
			});
		});

		return { labels, datasets };
	}

	/**
	 * Get the API endpoint based on the currently selected options.
	 * @param {string|null} [tool] When applicable.
	 * @returns {string}
	 */
	getEndpoint(tool = null) {
		let endpoint = this.API_URL;
		if (this.metric === 'visitors') {
			endpoint += '/unique';
		}
		if (this.$toolsInput.val().length) {
			endpoint += `/tool/${tool}`;
		}
		if (this.singleDate) {
			endpoint += `/day/${this.date}`;
		} else {
			endpoint += `/daily/${this.startDate}/${this.endDate}`;
		}
		return endpoint;
	}

	sortByValues(results) {
		const sortedResults = {};
		this.max = 0;
		const sortedTools = Object.keys(results).sort((a, b) => {
			this.max = Math.max(this.max, results[a], results[b]);
			if (results[a] < results[b]) {
				return 1;
			}
			if (results[a] > results[b]) {
				return -1;
			}
			return 0;
		});
		sortedTools.forEach(tool => {
			sortedResults[tool] = results[tool];
		});
		return sortedResults;
	}

	/**
	 * Update the view.
	 * @param {Object} data
	 */
	showData(data) {
		this.$output.html('');
		if (this.chartType === 'waterfall') {
			this.$outputTable.removeClass('hidden');
			this.$chartWrapper.addClass('hidden');
			return this.showDataWaterfall(data);
		}

		this.updateSummaryTable(data);
		this.$outputTable.addClass('hidden');
		this.$chartWrapper.removeClass('hidden');
		this.setLoading(false);

		if (this.chart && this.chartType !== this.prevChartType) {
			// Need to destroy and redraw.
			this.chart.destroy();
			this.prevChartType = this.chartType;
		} else if (this.chart) {
			// If it's just data that's changing, we don't need to create a new Chart.
			this.chart.data = data;
			this.chart.update();
			return;
		}

		this.chart = new Chart(this.$chart[0], {
			type: this.chartType,
			data,
			options: {
				scales: {
					y: {
						beginAtZero: true,
					}
				},
				interaction: {
					intersect: false,
					mode: 'index',
				},
				plugins: {
					legend: {
						display: this.tools.length > 1,
					}
				}
			}
		});
	}

	/**
	 * Update the summary table shown below the chart.
	 * @param {Object} data
	 */
	updateSummaryTable(data) {
		if (data.datasets.length === 1) {
			// When have just one dataset, we don't need to show a table.
			this.$summaryTable.hide();
			this.$singleToolSummary.html(`
				<a href="https://toolsadmin.wikimedia.org/tools/" target="_blank">All tools</a>
				&bull;
				<span class="text-muted">${this.startDate} – ${this.endDate}</span>
				&bull;
				<strong>${data.datasets[0].sum.toLocaleString()} hits</strong>
			`);
			return;
		}

		this.$singleToolSummary.hide();
		this.$summaryTable.show();

		// Sort datasets by hit count.
		data.datasets.sort((a, b) => {
			if (a.sum < b.sum) {
				return 1;
			}
			if (a.sum > b.sum) {
				return -1;
			}
			return 0;
		});
		this.$summaryOutput.html('');
		const numDays = data.labels.length;

		// Multi-dataset table.
		data.datasets.forEach((dataset, index) => {
			const background = config.chartColors[index % config.chartColors.length];
			this.$summaryOutput.append(`<tr>
				<td><span class="summary-table--color-block" style="background:${background}"></span></td>
				<td>${this.getHtmlForToolLink(dataset.label)}</td>
				<td>${dataset.sum.toLocaleString()}</td>
				<td>${Math.round(dataset.sum / numDays).toLocaleString()}</td>
				<td>
					<a target="_blank" href="https://toolsadmin.wikimedia.org/tools/id/${dataset.label}">Toolsadmin</a>
					&bull;
					<a target="_blank" href="https://toolhub.wikimedia.org">Toolhub</a>
				</td>
			</tr>`);
		});
	}

	/**
	 * Get the labels to be used on the x-axis of the chart.
	 * @returns {Array<string>}
	 */
	getChartDateLabels() {
		const startDate = new Date(this.startDate),
			endDate = new Date(this.endDate),
			labels = [];

		while (startDate <= endDate) {
			labels.push(this.formatDate(startDate));
			startDate.setDate(startDate.getDate() + 1);
		}

		return labels;
	}

	/**
	 * Update the rows of the waterfall chart.
	 * @param {Object} results
	 */
	showDataWaterfall(results) {
		Object.keys(results).forEach((tool, index) => {
			const width = 100 * (results[tool] / this.max),
				offsetTop = (index * 40) + 40;
			const style = `background:linear-gradient(to right, #EEE ${width}%, transparent ${width}%); top:${offsetTop}px`;
			this.$output.append(`<tr class="toolviews-entry">
					<th class="toolviews-entry--rank" scope="row">${index + 1}</th>
					<td>
						${this.getHtmlForToolLink(tool)}
						<span class="toolviews-entry--background" style="${style}"></span>
					</td>
					<td>
						<a href="#" class="toolviews-entry--link" data-tool="${tool}">${results[tool].toLocaleString()}</a>
					</td>
				</tr>`);

			setTimeout(() => {
				this.setLoading(false);
				$('.toolviews-entry--background').addClass('animate');
				$('.toolviews-entry--link').off('click')
					.on('click', e => {
						e.preventDefault();
						this.$toolsInput.val(this.$toolsInput.val().concat([e.target.dataset.tool]))
						this.$toolsInput.trigger('change');
						// Waterfall doesn't make sense for just one tool.
						this.$chartType.val('bar');
						this.$chartType.trigger('change');
					});
			}, 10);
		});
	}

	setLoading(state) {
		$('#main').toggleClass('loading', state);
		$('input, select').prop('disabled', state);
	}

	/**
	 * Update the URL with the currently selected params, so that direct links can be made to the results.
	 * @return {string} The search query string, for comparison with location.search to prevent duplicate queries.
	 */
	pushParams() {
		const params = new URLSearchParams({
			singleDate: this.singleDate ? 1 : 0,
			metric: this.metric,
			chartType: this.chartType
		});
		if (this.singleDate) {
			params.append('date', this.date);
		} else {
			params.append('start', this.startDate);
			params.append('end', this.endDate);
		}
		params.append(
			'tools',
			this.tools.join('|').replace(/[&%?+]/g, encodeURIComponent)
		);
		const queryStr = '?' + params.toString();
		window.history.replaceState({}, document.title, queryStr);
		return queryStr;
	}

	getHtmlForToolLink(tool) {
		return $('<a>', {
			text: tool,
			href: `https://${tool}.toolforge.org`,
			target: '_blank'
		})[0].outerHTML;
	}
}

$(() => {
	const toolviews = new Toolviews();
	toolviews.init();
});
